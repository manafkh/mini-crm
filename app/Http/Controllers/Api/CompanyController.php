<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Employee;
use App\Traits\Responsable;

class CompanyController extends Controller
{
    use Responsable;

    public function getAllAvilableEmployeesByCompanyId(string $id)
    {
        $employees = Employee::query()
                        ->where('company_id', '=', $id)->get();

        return $this->success(
            'get All Avilable Employees in company ID '.$id,
            [
                'employees'=>$employees
            ]
        );
    }

    public function getNumberOfInternedEmployees()
    {
        $internedEmployeesCount = Employee::query()
                                  ->where('is_intern', '=', true)->count();
        return $this->success(
                'get Number Of Interned Employees',
                [
                 'interned_employees_count'=>$internedEmployeesCount
                ]
            );
    }
}
