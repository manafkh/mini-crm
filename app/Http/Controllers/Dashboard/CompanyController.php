<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Company\CreateCompanyRequest;
use App\Http\Requests\Dashboard\Company\UpdateCompanyRequest;
use App\Mail\NewCompanyMail;
use App\Models\Company;
use App\Traits\UploadMedia;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class CompanyController extends Controller
{
    use UploadMedia;

    public function index(Request $request): View
    {
        $search = $request->input('search');
        $companiesQuery = Company::query();
        if ($search) {
            $companiesQuery->where('name', 'like', '%'.$search.'%')
                ->orWhere('email', 'like', '%'.$search.'%');
        }
        $companies = $companiesQuery->paginate(10);

        return view('dashboard.companies.index',
            ['companies' => $companies]
        );
    }

    public function create(): View
    {
        return view('dashboard.companies.create');
    }

    public function store(CreateCompanyRequest $request): RedirectResponse
    {
        $company = new company();
        $company->name = $request->validated('name');
        $company->email = $request->validated('email');
        $company->logo = $this->uploadImage('logo', 'company');
        $company->website_url = $request->validated('website_url');
        $company->save();
        Mail::to($company)->send(new NewCompanyMail($company));

        return redirect()->route('companies.index');
    }

    public function show(string $id): View
    {
        $company = Company::with('employees')->find($id);

        return view('dashboard.companies.show',
            ['company' => $company]
        );
    }

    public function edit(Company $company): View
    {
        return view('dashboard.companies.edit',
            ['company' => $company]
        );
    }

    public function update(UpdateCompanyRequest $request, Company $company): RedirectResponse
    {
        $company->name = $request->validated('name');
        $company->email = $request->validated('email');

        if ($request->hasfile('logo')) {
            $company->logo = $this->uploadImage('logo', 'company');
        }
        $company->website_url = $request->validated('website_url');
        $company->save();

        return redirect()->route('companies.index');
    }

    public function destroy(Company $company): RedirectResponse
    {
        $company->delete();

        return redirect()->route('companies.index');
    }
}
