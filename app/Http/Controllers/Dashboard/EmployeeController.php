<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Employee\CreateEmployeeRequest;
use App\Http\Requests\Dashboard\Employee\UpdateEmployeeRequest;
use App\Models\Company;
use App\Models\Employee;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): View
    {
        $search = $request->input('search');

        // Query to fetch employees
        $employeesQuery = Employee::query();

        // Apply search filter if search term is provided
        if ($search) {
            $employeesQuery->where('first_name', 'like', '%' . $search . '%')
                           ->orWhere('last_name', 'like', '%' . $search . '%')
                           ->orWhere('email', 'like', '%' . $search . '%');
        }
        $employees = $employeesQuery->paginate(10); // You can change the pagination limit as needed
        return view('dashboard.employees.index', ['employees' => $employees]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        $companies = Company::all();

        return view('dashboard.employees.create', ['companies' => $companies]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CreateEmployeeRequest $request): RedirectResponse
    {
        $employee = new Employee();
        $employee->company_id = $request->validated('company_id');
        $employee->first_name = $request->validated('first_name');
        $employee->last_name = $request->validated('last_name');
        $employee->email = $request->validated('email');
        $employee->phone_number = $request->validated('phone_number');
        $employee->save();

        return redirect()->route('employees.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id): view
    {
        $employee = Employee::find($id);

        return view('dashboard.employees.show', ['employee' => $employee]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Employee $employee): view
    {
        $companies = Company::all();
        return view('dashboard.employees.edit',
            [
                'companies' => $companies,
                'employee' => $employee,
            ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateEmployeeRequest $request, Employee $employee): RedirectResponse
    {
        $employee->company_id = $request->validated('company_id');
        $employee->first_name = $request->validated('first_name');
        $employee->last_name = $request->validated('last_name');
        $employee->email = $request->validated('email');
        $employee->phone_number = $request->validated('phone_number');
        $employee->save();

        return redirect()->route('employees.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id): RedirectResponse
    {
        $employee = Employee::find($id);
        $employee->delete();

        return redirect()->route('employees.index');
    }
}
