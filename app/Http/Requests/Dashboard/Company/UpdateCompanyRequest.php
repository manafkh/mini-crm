<?php

namespace App\Http\Requests\Dashboard\Company;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $company = $this->route()->company;
        return [
            'name'=>['required','string','max:50'],
            'email'=>['required','unique:users,email,'.$company->id,'string','max:50'],
            'logo'=>['nullable','image'],
            'website_url'=>['url','required','max:300']
        ];
    }
}
