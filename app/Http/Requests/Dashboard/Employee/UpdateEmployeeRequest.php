<?php

namespace App\Http\Requests\Dashboard\Employee;

use Illuminate\Foundation\Http\FormRequest;

class UpdateEmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $employee = $this->route()->employee;
        return [
            'company_id'=>['required'],
            'first_name'=>['required','string','max:50'],
            'last_name'=>['required','string','max:50'],
            'email'=>['required','email','max:50','unique:employees,email,'.$employee->id],
            'phone_number'=>['required','unique:employees,phone_number,'.$employee->id],
        ];
    }
}
