<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Employee extends Model
{
    use HasFactory;

    protected $fillable = [
        'company_id',
        'first_name',
        'last_name',
        'email',
        'phone_number',
        'is_intern',
        'started_at',
    ];

    protected $dates=[
        'started_at'
    ];

    public function company(): BelongsTo
    {
        return $this->belongsTo(Company::class);
    }

    public function getInternshipDurationAttribute(): int
    {
        if ($this->is_intern && $this->started_at) {
            $startedAt = Carbon::parse($this->started_at);
            $now = Carbon::now();
            return $startedAt->diffInDays($now);
        }
        return 0;
    }
}
