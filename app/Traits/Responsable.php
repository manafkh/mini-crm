<?php
namespace App\Traits;
use Illuminate\Support\Facades\Response;

trait Responsable{

    public function success($message = null, $content = [], $status = 200){
        return Response::json([
            'message' => $message,
            'content' => $content,
            'status' => $status,
        ], $status);
    }

    public function error($message, int $status = 500){
        return Response::json([
            'message' => $message,
            'status' => $status,
        ], $status);
    }
}
