<?php
namespace App\Traits;

use Illuminate\Support\Facades\Storage;

trait UploadMedia
{

    function uploadImage(string $key, string $folder)
    {
        $request = \request();
        if ($request->hasFile($key)) {
          return  Storage::disk('public')->put($folder, $request->file($key));
        }
        return null;
    }
}
