@extends('dashboard.index')
@section('content')
    <div class="container px-5 mt-4">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="card">
            <div class="card-header d-flex">
                <h3 class="text-xl text-gray font-weight-bold">Add new Company</h3>
            </div>
            <div class="card-body">
                <form action="{{route('companies.store')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" placeholder="name" class="form-control" name="name">
                    </div>
                    <div class="form-group mt-2">
                        <label>Email</label>
                        <input type="email" placeholder="email" class="form-control " name="email">
                    </div>
                    <div class="form-group mt-2">
                        <label>Link</label>
                        <input type="link" placeholder="website url" class="form-control" name="website_url">
                    </div>

                    <div class="form-group ">
                        <label>Logo</label>
                        <input type="file" placeholder="Logo" class="form-control" name="logo">
                    </div>

                    <div class="form-group mt-2">
                        <input type="submit" value="Add Company" class="btn btn-primary my-4 rounded">
                    </div>
                </form>
            </div>
        </div>
    </div>


    @endsection
