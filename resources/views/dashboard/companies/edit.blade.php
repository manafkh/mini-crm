@extends('dashboard.index')
@section('content')
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Update current Company</h4>
            </div>
            <div class="card-body">
                <form action="{{ route('companies.update', $company->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" placeholder="name" class="form-control" name="name"
                            value="{{ $company->name ? $company->name : '' }}">
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" placeholder="Email Address" class="form-control" name="email"
                            value="{{ $company->email ? $company->email : '' }}">
                    </div>
                    <div class="form-group">
                        <label>Website url</label>
                        <input type="text" placeholder="Website url" class="form-control" name="website_url"
                            value="{{ $company->website_url ? $company->website_url : '' }}">
                    </div>
                    <div class="form-group">
                        <label>Logo</label>
                        <input type="file" placeholder="Logo" class="form-control" name="logo">
                    </div>
                    <div class="form-group">
                        <input type="submit" value="Update Company" class="btn btn-primary">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
