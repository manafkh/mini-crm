@extends('dashboard.index')
@section('content')
    <div class="container px-5 mt-4">
        <div class="card">
            <div class="card-header d-flex justify-content-between">
                <h3 class="text-xl text-gray font-weight-bold">Companies Info</h3>
                <form action="{{ route('companies.index') }}" method="GET" class="form-inline">
                    <input type="text" name="search" class="form-control mr-2" placeholder="Search...">
                    <button type="submit" class="btn btn-primary">Search</button>
                </form>
            </div>
            <div class="card-body">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>company name</th>
                                    <th>Email</th>
                                    <th class="text-right">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($companies as $key => $company)
                                    <tr style="vertical-align: center">
                                        <td>{{ $company->id }}</td>
                                        <td>{{ $company->name }}</td>
                                        <td>{{ $company->email }}</td>

                                        <td class="d-flex justify-content-end">
                                            <a href="{{ route('companies.edit', $company->id) }}"> <button
                                                    class="btn btn-primary  rounded mr-1"
                                                    style="width: 4.5rem">Edit</button>
                                            </a>
                                            <a href="{{ route('companies.show', $company->id) }}"> <button
                                                    class="btn btn-info rounded px-2 mr-1"
                                                    style="width: 4.5rem">Show</button>
                                            </a>
                                            <form method="post"
                                                action="{{ route('companies.destroy', ['company' => $company->id]) }}"
                                                class="inline">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <button class="btn btn-danger rounded"style="width: 4.5rem">Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    {{ $companies->links() }}
                </div>

            </div>
        </div>
    </div>
@endsection
