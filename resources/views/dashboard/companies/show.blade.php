@extends('dashboard.index')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <div class="card">
                <div class="card-header">
                    <h3>Company Information</h3>
                </div>
                <div class="card-body">
                    <div class="mb-3">
                        <label class="form-label">Name:</label>
                        <p>{{ $company->name }}</p>
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Email:</label>
                        <p>{{ $company->email }}</p>
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Logo:</label>
                        <img src="{{ $company->logo_url }}" alt="Company Logo" class="img-fluid">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Website URL:</label>
                        <p>{{ $company->website_url }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-md-6 offset-md-3">
            <div class="card">
                <div class="card-header">
                    <h3>Employees</h3>
                </div>
                <div class="card-body">
                    <ul class="list-group">
                        @foreach ($company->employees as $employee)
                        <li class="list-group-item">
                            {{ $employee->first_name }} {{ $employee->last_name }}
                            <br>
                            Email: {{ $employee->email }}
                            <br>
                            Phone Number: {{ $employee->phone_number }}
                            <br>
                            Intern: {{ $employee->is_intern ? 'Yes' : 'No' }}
                            <br>
                            Started At: {{ $employee->started_at }}
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
