@extends('dashboard.index')
@section('content')
    <div class="container px-5 mt-4">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="card">
            <div class="card-header d-flex">
                <h3 class="text-xl text-gray font-weight-bold">Add new Employee</h3>
            </div>
            <div class="card-body">
                <form action="{{route('employees.store')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label>First name</label>
                        <input type="text" placeholder="first name" class="form-control" name="first_name">
                    </div>
                    <div class="form-group">
                        <label>Last name</label>
                        <input type="text" placeholder="last name" class="form-control" name="last_name">
                    </div>
                    <div class="form-group mt-2">
                        <label>Email</label>
                        <input type="email" placeholder="email" class="form-control " name="email">
                    </div>
                    <div class="form-group mt-2">
                        <label>Phone number</label>
                        <input type="text" placeholder="Phone number" class="form-control" name="phone_number">
                    </div>
                    <div class="form-group mt-2">
                        <label>Company</label>
                        <select class="form-control" id="company" name="company_id" >
                            <option value="">chooes company</option>
                            @foreach($companies as $company)
                            <option value="{{ $company->id }}">{{ $company->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group mt-2">
                        <input type="submit" value="Add Employee" class="btn btn-primary my-4 rounded">
                    </div>
                </form>
            </div>
        </div>
    </div>


    @endsection
