@extends('dashboard.index')
@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Update current employee</h4>
            </div>
            <div class="card-body">
                <form action="{{ route('employees.update', $employee->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label>First Name</label>
                        <input type="text" placeholder="first name" class="form-control" name="first_name"
                            value="{{ $employee->first_name ? $employee->first_name : '' }}">
                    </div>
                    <div class="form-group">
                        <label>last name</label>
                        <input type="last_name" placeholder="last name" class="form-control" name="last_name"
                            value="{{ $employee->last_name ? $employee->last_name : '' }}">
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" placeholder="Email Address" class="form-control" name="email"
                            value="{{ $employee->email ? $employee->email : '' }}">
                    </div>
                    <div class="form-group">
                        <label>Phone number</label>
                        <input type="text" placeholder="Phone" class="form-control" name="phone_number"
                            value="{{ $employee->phone_number ? $employee->phone_number : '' }}">
                    </div>
                    <div class="form-group mt-2">
                     <label>Company</label>
                     <select id="company" class="form-control" name="company_id">
                        @foreach ($companies as $company)
                            <option value="{{ $company->id }}" @selected($employee->company->id)>{{ $company->name }}</option>
                        @endforeach
                     </select>
                    </div>
                    <div class="form-group">
                        <input type="submit" value="Update employee" class="btn btn-primary">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
