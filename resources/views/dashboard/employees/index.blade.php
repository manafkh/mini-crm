@extends('dashboard.index')
@section('content')
    <div class="container px-5 mt-4">
        <div class="card">
            <div class="card-header d-flex justify-content-between">
                <h3 class="text-xl text-gray font-weight-bold">Employees Info</h3>
                <form action="{{ route('employees.index') }}" method="GET" class="form-inline">
                    <input type="text" name="search" class="form-control mr-2" placeholder="Search...">
                    <button type="submit" class="btn btn-primary">Search</button>
                </form>
            </div>
            <div class="card-body">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th> First Name</th>
                                    <th> Last Name</th>
                                    <th> Email</th>
                                    <th>Phone Number</th>
                                    <th>Company</th>
                                    <th class="text-right">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($employees as $key => $employee)
                                    <tr style="vertical-align: center">
                                        <td>{{ $employee->id }}</td>
                                        <td>{{ $employee->first_name }}</td>
                                        <td>{{ $employee->last_name }}</td>
                                        <td>{{ $employee->email }}</td>
                                        <td>{{ $employee->phone_number }}</td>
                                        <td>{{ $employee->company->name }}</td>

                                        <td class="d-flex justify-content-end">
                                            <a href="{{ route('employees.edit', $employee->id) }}"> <button
                                                    class="btn btn-primary  rounded mr-1" style="width: 4.5rem">Edit</button>
                                            </a>
                                            <a href="{{ route('employees.show', $employee->id) }}"> <button
                                                    class="btn btn-info rounded px-2 mr-1"
                                                    style="width: 4.5rem">Show</button>
                                            </a>
                                            <form method="post" action="{{ route('employees.destroy', ['employee' => $employee->id]) }}" class="inline">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <button class="btn btn-danger rounded"style="width: 4.5rem">Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    {{ $employees->links() }}
                </div>

            </div>
        </div>
    </div>
@endsection
