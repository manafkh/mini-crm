@extends('dashboard.index')
@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <h1 class="card-title">Employee Information</h1>
            </div>
            <div class="card-body">
                <ul class="list-group">
                    <li class="list-group-item"><strong>Company ID:</strong> {{ $employee->company_id }}</li>
                    <li class="list-group-item"><strong>First Name:</strong> {{ $employee->first_name }}</li>
                    <li class="list-group-item"><strong>Last Name:</strong> {{ $employee->last_name }}</li>
                    <li class="list-group-item"><strong>Email:</strong> {{ $employee->email }}</li>
                    <li class="list-group-item"><strong>Phone Number:</strong> {{ $employee->phone_number }}</li>
                    <li class="list-group-item"><strong>Is Intern:</strong> {{ $employee->is_intern ? 'Yes' : 'No' }}</li>
                    <li class="list-group-item"><strong>Started At:</strong> {{ $employee->started_at }}</li>
                </ul>
            </div>
        </div>
    </div>
@endsection
