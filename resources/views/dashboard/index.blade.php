<!doctype html>
<html lang="en">
<head>
@include('dashboard.layouts.partials._meta')
</head>
<body dir="ltr">
@include('dashboard.layouts.base._sidebar')
<div class="page">
    @include('dashboard.layouts.base._navbar')

    @yield('content')

</div>
@include('dashboard.layouts.partials._scripts')

</body>
</html>
