<!-- Side Navbar -->
<nav class="side-navbar">
    <div class="side-navbar-wrapper">
        <!-- Sidebar Header    -->
        <div class="sidenav-header d-flex align-items-center justify-content-center">
            <!-- User Info-->
            <div class="sidenav-header-inner text-center">
                <h2 class="h5">{{ auth()->user()->name }}</h2><span>System Admin</span>
            </div>
            <!-- Small Brand information, appears on minimized sidebar-->
            <div class="sidenav-header-logo"><a href="/index.html" class="brand-small text-center">
                    <strong>B</strong><strong class="text-primary">D</strong></a></div>
        </div>
        <!-- Sidebar Navigation Menus-->
        <div class="main-menu">
            <h5 class="sidenav-heading">Main</h5>
            <ul id="side-main-menu" class="side-menu list-unstyled">
                <li><a href="#companiesdropdownDropdown" aria-expanded="false" data-toggle="collapse"> <i
                            class="icon-user"></i>companies </a>
                    <ul id="companiesdropdownDropdown" class="collapse list-unstyled ">
                        <li><a href="{{ route('companies.create') }}">Add company</a></li>
                        <li><a href="{{ route('companies.index') }}">Show companies</a></li>
                    </ul>
                </li>
                <li><a href="#employeesdropdownDropdown" aria-expanded="false" data-toggle="collapse"> <i
                            class="icon-padnote"></i>{{ __('Employees') }}</a>
                    <ul id="employeesdropdownDropdown" class="collapse list-unstyled ">
                        <li><a href="{{ route('employees.create') }}">Add Employee</a></li>
                        <li><a href="{{ route('employees.index') }}">Show Employees</a></li>
                    </ul>
                </li>

            </ul>
        </div>

    </div>
</nav>
